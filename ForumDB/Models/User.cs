﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; 

namespace ForumDB.Models
{
    public class User
    {
        // ID юзера
        public int Id { get; set; }

        [Required]
        [Display(Name = "Имя пользователя")]
        [StringLength(100, ErrorMessage = "Имя пользователя", MinimumLength = 6)]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Пароль слишком короткий", MinimumLength = 6)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

       // [Required]
       // [DataType(DataType.Password)]
        //[StringLength(100, ErrorMessage = "Пароль слишком короткий", MinimumLength = 6)]
       // [Display(Name = "Подтвердить Пароль")]
       // [Compare("Password", ErrorMessage = "Пароли не совпадают")]
      //  public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Почта")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }
    }
}