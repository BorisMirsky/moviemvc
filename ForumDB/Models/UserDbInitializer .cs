﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ForumDB.Models
{
    public class UserDbInitializer : DropCreateDatabaseAlways<UsersContext>
    {
        protected override void Seed(UsersContext db)
        {
            db.Users.Add(new User { Login = "User1", Password = "Password1", Email = "user1@ya.hell" });
            db.Users.Add(new User { Login = "User2", Password = "Password2", Email = "user2@ya.hell" });
            db.Users.Add(new User { Login = "User3", Password = "Password3", Email = "user3@ya.hell" });

            base.Seed(db);
        }
    }
}