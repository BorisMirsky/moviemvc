﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumDB.Models;

namespace ForumDB.Controllers
{
    public class HomeController : Controller
    {
        // создаем контекст данных
        UsersContext db = new UsersContext();

        public ActionResult Index()
        {
            // получаем из бд все объекты Book
          //  IEnumerable<User> users = db.Users;
            // передаем все объекты в динамическое свойство Books в ViewBag
          //  ViewBag.Users = users;
            // возвращаем представление
            return View(db.Users);
        }

        [HttpGet]
        public ActionResult Login()
        {
            User user = new User();
            return View(user);
        }

        // [HttpPost]
        // public ActionResult Login(UserModel user)
        // {
        //     return Redirect("/Home/Index");
        // }

        [HttpPost]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
                return Redirect("/Home/Index");
            else
                return View(user);
        }

        [HttpGet]
        public ActionResult Registry()
        {
            User user = new User();
            return View(user);
        }

        [HttpPost]
        public ActionResult Registry(User user)
        {
            if (ModelState.IsValid)
                return Redirect("/Home/Index");
            else
                return View(user);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}